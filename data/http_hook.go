package data

import (
	"flow_analysis/bean"
	"flow_analysis/db"
	"flow_analysis/nsfw"
	"fmt"
	"os"
	"strings"
)

var predictor, _ = nsfw.NewLatestPredictor()

type HttpHook struct {
}

func (h *HttpHook) Handler(data *bean.HookData) {
	id := data.Net.FastHash()
	if data.IsReq {
		id = data.Net.Reverse().FastHash()
	}
	if data.IsReq {
		for s, _ := range data.Req.PostForm {
			fmt.Println(s)
		}
		for s, _ := range data.Req.Form {
			fmt.Println(s)
		}
		for s, _ := range data.Req.MultipartForm.Value {
			fmt.Println(s)
		}
		go db.Add(data.Net, id, data.Req)
	} else {

	}
	if data.FileDatas != nil {
		for i := range data.FileDatas {
			fileData := data.FileDatas[i]
			fmt.Println("文件名：" + fileData.FileName)
			os.WriteFile("./file/"+fileData.FileName, fileData.Bytes, 0644)

			go func() {
				defer func() {
					if err := recover(); err != nil {
						fmt.Println("鉴别图片失败：" + fileData.FileName)
					}
				}()
				lower := strings.ToLower(fileData.FileName)
				if strings.Contains(lower, "png") || strings.Contains(lower, "jpg") || strings.Contains(lower, "jpeg") {
					image := predictor.NewImage("./file/"+fileData.FileName, 3)
					fmt.Println("文件名：" + fileData.FileName + "检查结果：" + predictor.Predict(image).Describe())
				}
			}()
		}
	}
}
