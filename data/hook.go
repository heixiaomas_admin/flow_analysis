package data

import (
	"flow_analysis/bean"
)

type Hook interface {
	Handler(data *bean.HookData)
}
