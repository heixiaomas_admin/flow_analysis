package db

import (
	"gorm.io/gorm"
)

type ReqData struct {
	gorm.Model
	RequestId string
	Src       string
	Dst       string
	Host      string
	Url       string
	IP        string
	Method    string
}
