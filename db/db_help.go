package db

import (
	"github.com/google/gopacket"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

var db *gorm.DB

var reqDataArr = make([]*ReqData, 0)

func Init() {
	if db == nil {
		db, _ = gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
		db.AutoMigrate(&ReqData{})
	}
}

func Add(net gopacket.Flow, id uint64, r *http.Request) {
	Init()
	host := r.Host
	s := host + r.RequestURI
	if len(reqDataArr) >= 10 {
		db.Create(reqDataArr)
		//清空
		reqDataArr = reqDataArr[:0]
	}
	reqDataArr = append(reqDataArr, &ReqData{
		RequestId: strconv.FormatUint(id, 10),
		Dst:       net.Dst().String(),
		Src:       net.Src().String(),
		Host:      host,
		Url:       s,
		IP:        r.RemoteAddr,
		Method:    r.Method,
	})
}
