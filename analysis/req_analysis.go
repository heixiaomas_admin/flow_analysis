package analysis

import (
	"github.com/google/gopacket"
	"net/http"
)

type ReqAnalysis struct {
}

func (h *ReqAnalysis) Req(net gopacket.Flow, id uint64, request *http.Request) {

}

func (h *ReqAnalysis) Res(net gopacket.Flow, id uint64, response *http.Response) {

}
