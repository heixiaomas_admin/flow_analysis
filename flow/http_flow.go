package flow

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"flow_analysis/bean"
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/tcpassembly"
	"github.com/google/gopacket/tcpassembly/tcpreader"
	"github.com/google/uuid"
	"io"
	"mime"
	"net/http"
	"strings"
)

type httpRequest struct {
	dataFlowPacket *DataFlowPacket
}

type httpReqStream struct {
	net, transport gopacket.Flow
	r              tcpreader.ReaderStream
	isReq          bool
	seqId          string
	dataFlowPacket *DataFlowPacket
}

func (h *httpRequest) New(net, transport gopacket.Flow) tcpassembly.Stream {
	hstream := &httpReqStream{
		isReq:          transport.Dst().String() == "80",
		net:            net,
		transport:      transport,
		r:              tcpreader.NewReaderStream(),
		dataFlowPacket: h.dataFlowPacket,
	}
	go hstream.run()
	return &hstream.r
}

func (h *httpReqStream) run() {
	buf := bufio.NewReader(&h.r)
	for {
		if h.isReq {
			req, err := http.ReadRequest(buf)
			if err == io.EOF {
				return
			} else if err != nil {
				return
			} else {
				h.req(req)
				tcpreader.DiscardBytesToEOF(req.Body)
				req.Body.Close()
			}
		} else {
			res, err := http.ReadResponse(buf, nil)
			if err == io.EOF {
				return
			} else if err != nil {
				return
			} else {
				h.res(res)
				tcpreader.DiscardBytesToEOF(res.Body)
				res.Body.Close()
			}
		}
	}
}

func (h *httpReqStream) req(request *http.Request) {

	for s, _ := range request.PostForm {
		fmt.Println(s)
	}
	for s, _ := range request.Form {
		fmt.Println(s)
	}

	reader, err := request.MultipartReader()
	data := make([]*bean.FileData, 0)
	if err == nil {
		// 遍历处理每个文件
		for {
			part, err := reader.NextPart()
			if err == io.EOF {
				break
			}
			if err != nil {
				return
			}
			readAll, err := io.ReadAll(part)
			if err != nil {
				continue
			}
			if len(part.FileName()) == 0 {
				continue
			}
			data = append(data, &bean.FileData{
				FileName: part.FileName(),
				Bytes:    readAll,
			})
			part.Close() // 关闭当前part，释放资源
		}
	}
	all, err1 := io.ReadAll(request.Body)
	if err1 != nil {
		return
	}
	h.dataFlowPacket.AnalysisQueue.PushData(&bean.HookData{
		IsReq:     true,
		Net:       h.net,
		Req:       request,
		Bytes:     all,
		FileDatas: data,
	})
}

func (h *httpReqStream) res(response *http.Response) {
	body, err := io.ReadAll(response.Body)
	if err != nil {
		return
	}
	encoding := response.Header["Content-Encoding"]
	if len(encoding) > 0 && (encoding[0] == "gzip" || encoding[0] == "deflate") {
		var r io.Reader
		r = bytes.NewBuffer(body)
		r, err := gzip.NewReader(r)
		if err != nil {
			return
		}
		all, err := io.ReadAll(r)
		if err == nil {
			body = all
		}
	}

	data := make([]*bean.FileData, 0)
	//普通检查
	contentDisposition := response.Header.Get("Content-Disposition")
	if contentDisposition != "" {
		_, params, err := mime.ParseMediaType(contentDisposition)
		if err != nil {
			return
		}
		filename, ok := params["filename"]
		if ok {
			data = append(data, &bean.FileData{
				FileName: filename,
				Bytes:    body,
			})
		}
	}
	//类型检查
	contentType := response.Header.Get("Content-Type")
	if len(data) == 0 && strings.HasPrefix(contentType, "image/") || strings.HasPrefix(contentType, "audio/") || strings.HasPrefix(contentType, "video/") {
		extension := getExtension(contentType)
		if extension != "" {
			data = append(data, &bean.FileData{
				FileName: uuid.New().String() + extension,
				Bytes:    body,
			})
		}
	}
	h.dataFlowPacket.AnalysisQueue.PushData(&bean.HookData{
		IsReq:     false,
		Net:       h.net,
		Res:       response,
		Bytes:     body,
		FileDatas: data,
	})

}
func getExtension(contentType string) string {
	switch contentType {
	case "image/jpeg":
		return ".jpg"
	case "image/png":
		return ".png"
	case "image/gif":
		return ".gif"
	case "audio/mpeg":
		return ".mp3"
	case "audio/wav":
		return ".wav"
	case "video/mp4":
		return ".mp4"
	case "video/quicktime":
		return ".mov"
	default:
		return ""
	}
}
