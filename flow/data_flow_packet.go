package flow

import (
	"flow_analysis/data"
	"flow_analysis/queue"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/tcpassembly"
	"log"
)

type DataFlowPacket struct {
	AnalysisQueue queue.AnalysisQueue
}

func NewFlowPacket(name string) {
	d := &DataFlowPacket{
		queue.NewAnalysisQueue(5, &data.HttpHook{}),
	}
	d.http(name)
}

func (receiver *DataFlowPacket) http(name string) {

	handle, err := pcap.OpenLive(name, 65535, true, pcap.BlockForever)
	if err != nil {
		log.Fatal(err)
	}
	defer handle.Close()

	// 设置捕获过滤器，只捕获HTTP数据包
	filter := "tcp and port 80"
	err = handle.SetBPFFilter(filter)
	if err != nil {
		log.Fatal(err)
	}
	streamFactoryReq := &httpRequest{receiver}
	streamPoolReq := tcpassembly.NewStreamPool(streamFactoryReq)
	assemblerReq := tcpassembly.NewAssembler(streamPoolReq)
	log.Println("reading in packets")
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	packets := packetSource.Packets()
	for {
		select {
		case packet := <-packets:
			if packet == nil {
				return
			}
			if packet.NetworkLayer() == nil || packet.TransportLayer() == nil || packet.TransportLayer().LayerType() != layers.LayerTypeTCP {
				continue
			}
			tcp := packet.TransportLayer().(*layers.TCP)
			assemblerReq.Assemble(packet.NetworkLayer().NetworkFlow(), tcp)
		}
	}

}
