package main

import (
	"flag"
	"flow_analysis/flow"
)

func main() {
	var intface string
	//命令行参数模式
	flag.StringVar(&intface, "net", "eth0", "网卡设备")
	flag.Parse()
	flow.NewFlowPacket(intface)
}
