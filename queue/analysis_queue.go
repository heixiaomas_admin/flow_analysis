package queue

import (
	"flow_analysis/bean"
	"flow_analysis/data"
	"fmt"
)

type AnalysisQueue struct {
	MessageChannel chan *bean.HookData
	queueSize      int
	Handle         data.Hook
}

// NewAnalysisQueue 事例一个
func NewAnalysisQueue(queueSize int, hook data.Hook) AnalysisQueue {
	queue := AnalysisQueue{
		make(chan *bean.HookData, queueSize),
		queueSize,
		hook,
	}
	queue.handel()
	return queue
}

// PushData 队列里发送数据
func (receiver AnalysisQueue) PushData(data *bean.HookData) {
	receiver.MessageChannel <- data
}

// Close 关闭通道
func (receiver AnalysisQueue) Close() {
	close(receiver.MessageChannel)
}

// handel 处理通道数据
func (receiver AnalysisQueue) handel() {
	for i := 0; i < receiver.queueSize; i++ {
		go func() {
			for {
				select {
				case num, ok := <-receiver.MessageChannel:
					if !ok { // 通道已关闭
						fmt.Println("通道关闭:", num)
						return
					}
					receiver.Handle.Handler(num)
				}
			}
		}()
	}
}
