package bean

import (
	"github.com/google/gopacket"
	"net/http"
)

type FileData struct {
	FileName string
	Bytes    []byte
}

type HookData struct {
	IsReq          bool
	Net, transport gopacket.Flow
	Bytes          []byte //Req或者Body内容
	FileDatas      []*FileData
	Req            *http.Request
	Res            *http.Response
}
